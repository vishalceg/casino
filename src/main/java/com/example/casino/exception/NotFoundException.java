package com.example.casino.exception;

public class NotFoundException extends RuntimeException{
	
	public NotFoundException(String id, String type) {
		super(type+":"+id+" not found.");
	}

}
