package com.example.casino.bets;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;

import com.example.casino.casinos.Casino;
import com.example.casino.games.Game;
import com.example.casino.users.User;

@Entity
@Table(name = "Bets")
public class Bet {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "game_id", referencedColumnName = "id", nullable = false)
	private Game game;

	@ManyToOne
	@JoinColumn(name = "casino_id", referencedColumnName = "id", nullable = false)
	private Casino casino;

	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
	private User user;

	@Column(nullable = false)
	private BetNumber betNumber;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public Casino getCasino() {
		return casino;
	}

	public void setCasino(Casino casino) {
		this.casino = casino;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public BetNumber getBetNumber() {
		return betNumber;
	}

	public void setBetNumber(BetNumber betNumber) {
		this.betNumber = betNumber;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	@Min(1)
	private int amount;

}
