package com.example.casino.bets;

public enum BetNumber {
	one(1), two(2), three(3), four(4), five(5), six(6), seven(7), eight(8), nine(9), ten(10), eleven(11), twelve(12),
	thirteen(13), forteen(14), fifteen(15), sixteen(16), seventeen(17), eighteen(18), nineteen(19), twenteen(20),
	twentyone(21), twentytwo(12), twentythree(13), twentyfour(24), twentyfive(15), twentysix(26), twentyseven(27),
	twentyeight(28), twentynine(29), thirty(30), thirtyone(31), thirtytwo(32), thirtythree(33), thirtyfour(34),
	thirtyfive(35), thirtysix(36);

	private int i;

	private BetNumber(int i) {
		this.i = i;
	}

	public int value() {
		return i;
	}

	public static BetNumber getFor(int value) {
		for (BetNumber b : BetNumber.values()) {
			if (b.i == value) {
				return b;
			}
		}
		return null;
	}

}
