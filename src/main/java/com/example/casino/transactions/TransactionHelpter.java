package com.example.casino.transactions;

import com.example.casino.util.DateUtil;

public class TransactionHelpter {

	public static Transaction getTxn(EntityType entityType, Long entityId, Type type, Integer amount) {
		Transaction txn = new Transaction();
		txn.setAmount(amount);
		txn.setEntityId(entityId);
		txn.setEntityType(entityType);
		txn.setType(type);
		txn.setStartTime(DateUtil.sqlDateNow());
		return txn;
	}

}
