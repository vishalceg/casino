package com.example.casino.games;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.springframework.data.annotation.CreatedDate;

import com.example.casino.casinos.Casino;
import com.example.casino.dealers.Dealer;

@Entity
@Table(name = "Games")
public class Game {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "dealer_id", referencedColumnName = "id", nullable = false)
	private Dealer dealer;

	@ManyToOne
	@JoinColumn(name = "casino_id", referencedColumnName = "id", nullable = false)
	private Casino casino;

	@CreatedDate
	@Column(name = "start_time")
	private Date startTime;
	@Min(0)
	@Max(36)
	@Column(columnDefinition = "integer default 0")
	private int diceResult;
	@Column(name = "dice_rolled_at")
	private Date diceRolledAt;

	@Column(name = "end_time")
	private Date endTime;

	@Enumerated(EnumType.STRING)
	private Status status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Casino getCasino() {
		return casino;
	}

	public void setCasino(Casino casino) {
		this.casino = casino;
	}

	public Dealer getDealer() {
		return dealer;
	}

	public void setDealer(Dealer dealer) {
		this.dealer = dealer;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public int getDiceResult() {
		return diceResult;
	}

	public void setDiceResult(int diceResult) {
		this.diceResult = diceResult;
	}

	public Date getDiceRolledAt() {
		return diceRolledAt;
	}

	public void setDiceRolledAt(Date diceRolledAt) {
		this.diceRolledAt = diceRolledAt;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}
