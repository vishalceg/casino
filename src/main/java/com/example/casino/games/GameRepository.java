package com.example.casino.games;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.casino.casinos.Casino;
import com.example.casino.dealers.Dealer;

@Repository("gameRepository")
public interface GameRepository extends JpaRepository<Game, Long> {

	Game findByDealerAndStatus(Dealer dealer, Status status);

	List<Game> findByCasinoAndStatus(Casino casino, Status status);

}
