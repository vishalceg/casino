package com.example.casino.util;

import java.sql.Date;
import java.util.Calendar;

public class DateUtil {
	
	public static Date sqlDateNow() {
		Calendar calendar = Calendar.getInstance();
		return new java.sql.Date(calendar.getTime().getTime());
	}

}
