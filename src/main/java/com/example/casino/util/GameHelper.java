package com.example.casino.util;

import java.util.Random;

import com.example.casino.bets.BetNumber;

public class GameHelper {
	
	public static int getRandomNum() {
		Random objGenerator = new Random();
		return objGenerator.nextInt(36);
	}
	
	
	public static boolean isOwn(int result, BetNumber expected) {
		return result == expected.value();
	}

}
