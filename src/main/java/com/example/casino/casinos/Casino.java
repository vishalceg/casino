package com.example.casino.casinos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Casinos")
public class Casino {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String name;
	private String ownercontact;//
	private String ownerName;
	@Column(columnDefinition = "integer default 0")
	private int balance;// cent value(So that we will be able to handle decimal point )- 1.50- db
						// value(150)

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public String getOwnercontact() {
		return ownercontact;
	}

	public void setOwnercontact(String ownercontact) {
		this.ownercontact = ownercontact;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

}
