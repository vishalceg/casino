package com.example.casino.casinos;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.example.casino.dealers.Dealer;
import com.example.casino.dealers.DealerInput;
import com.example.casino.dealers.DealerRepository;
import com.example.casino.exception.NotFoundException;
import com.example.casino.transactions.EntityType;
import com.example.casino.transactions.Transaction;
import com.example.casino.transactions.TransactionHelpter;
import com.example.casino.transactions.TransactionRepository;
import com.example.casino.transactions.Type;

@RestController
@RequestMapping("/api/v1")
public class CasinoController {

	@Autowired
	private CasinoRepository casinoRepository;
	@Autowired
	private DealerRepository dealerRepository;

	@Autowired
	private TransactionRepository transactionRepository;

	@GetMapping("/casinos")
	public List<Casino> getAllUsers() {
		return casinoRepository.findAll();
	}

	@PostMapping("/casinos/registration")
	public Casino registration(@RequestBody @Valid Casino casino) {
		return casinoRepository.save(casino);
	}

	@PostMapping("/casinos/{cid}/dealers")
	public Dealer addDealer(@PathVariable long cid, @RequestBody @Valid DealerInput dealerInput) {
		Optional<Casino> casino = casinoRepository.findById(cid);
		if (!casino.isPresent()) {
			throw new NotFoundException(String.valueOf(cid), "Casino");
		}
		Dealer dealer = new Dealer();
		dealer.setName(dealerInput.getName());
		dealer.setCasino(casino.get());
		return dealerRepository.save(dealer);
	}

	@GetMapping("/casinos/{cid}/dealers")
	public List<Dealer> fetchAllDealer(@PathVariable long cid) {
		Optional<Casino> casino = casinoRepository.findById(cid);
		if (!casino.isPresent()) {
			throw new NotFoundException(String.valueOf(cid), "Casino");
		}
		return dealerRepository.findByCasino(casino.get());
	}

	@PostMapping("/casinos/{cid}/recharge")
	public Casino recharge(@PathVariable long cid, @RequestParam Integer amount) {
		Optional<Casino> casino = casinoRepository.findById(cid);
		if (!casino.isPresent()) {
			throw new NotFoundException(String.valueOf(cid), "Casino");
		}
		if (amount < 1) {
			throw new RuntimeException("Amount should be greater than 0.");// Need to handle api exception
		}
		Transaction txn = TransactionHelpter.getTxn(EntityType.casino, casino.get().getId(), Type.recharge, amount);
		txn.setCasino(casino.get());
		transactionRepository.save(txn);
		casino.get().setBalance(amount);

		return casinoRepository.save(casino.get());
	}

}
