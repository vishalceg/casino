package com.example.casino.casinos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("casinoRepository")
public interface CasinoRepository extends JpaRepository<Casino, Long> {

}
