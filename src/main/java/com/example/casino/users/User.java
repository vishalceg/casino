package com.example.casino.users;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.example.casino.casinos.Casino;

@Entity
@Table(name = "Users")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String name;
	private int age;
	private String email;

	public Casino getCurrentCasino() {
		return currentCasino;
	}

	public void setCurrentCasino(Casino currentCasino) {
		this.currentCasino = currentCasino;
	}

	@ManyToOne
	@JoinColumn(name = "casino_id", referencedColumnName = "id")
	private Casino currentCasino;

	@Column(columnDefinition = "integer default 0")
	private int balance;// cent value(So that we will be able to handle decimal point )- 1.50- db
						// value(150)

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
