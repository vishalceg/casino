package com.example.casino.users;

import com.example.casino.bets.Bet;

public class BetOut {
	private Long gameId;
	private int betNumber;
	private int amount;

	public Long getGameId() {
		return gameId;
	}

	public void setGameId(Long gameId) {
		this.gameId = gameId;
	}

	public int getBetNumber() {
		return betNumber;
	}

	public void setBetNumber(int betNumber) {
		this.betNumber = betNumber;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public static BetOut getResp(Bet bet) {
		BetOut out = new BetOut();
		out.setAmount(bet.getAmount());
		out.setBetNumber(bet.getBetNumber().value());
		out.setGameId(bet.getGame().getId());
		return out;
	}

}
