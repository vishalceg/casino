package com.example.casino.users;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.casino.bets.Bet;
import com.example.casino.bets.BetNumber;
import com.example.casino.bets.BetRepository;
import com.example.casino.casinos.Casino;
import com.example.casino.casinos.CasinoRepository;
import com.example.casino.exception.NotFoundException;
import com.example.casino.games.Game;
import com.example.casino.games.GameRepository;
import com.example.casino.games.Status;
import com.example.casino.transactions.EntityType;
import com.example.casino.transactions.Transaction;
import com.example.casino.transactions.TransactionHelpter;
import com.example.casino.transactions.TransactionRepository;
import com.example.casino.transactions.Type;

@RestController
@RequestMapping("/api/v1")
public class UserController {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private CasinoRepository casinoRepository;

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private TransactionRepository transactionRepository;

	@Autowired
	private BetRepository betRepository;

	@GetMapping("/users")
	public List<User> getAllUsers() {
		return userRepository.findAll();
	}

	@PostMapping("/users/registration")
	public User registration(@RequestBody @Valid UserInput userInput) {
		User user = new User();
		user.setName(userInput.getName());
		user.setEmail(userInput.getEmail());
		user.setAge(userInput.getAge());
		return userRepository.save(user);
	}

	@PostMapping("/users/{uid}/recharge")
	public User recharge(@PathVariable long uid, @RequestParam Integer amount) {
		Optional<User> user = userRepository.findById(uid);
		if (!user.isPresent()) {
			throw new NotFoundException(String.valueOf(uid), "User");
		}
		if (amount < 1) {
			throw new RuntimeException("Amount should be greater than 0.");// Need to handle api exe
		}
		Transaction txn = TransactionHelpter.getTxn(EntityType.user, user.get().getId(), Type.recharge, amount);
		transactionRepository.save(txn);
		user.get().setBalance(amount);
		return userRepository.save(user.get());
	}

	@PostMapping("/users/{uid}/join_casino")
	public User recharge(@PathVariable long uid, @RequestParam Long casino_id) {
		Optional<User> user = userRepository.findById(uid);
		if (!user.isPresent()) {
			throw new NotFoundException(String.valueOf(uid), "User");
		}
		Optional<Casino> casino = casinoRepository.findById(casino_id);
		if (!casino.isPresent()) {
			throw new NotFoundException(String.valueOf(casino_id), "Casino");
		}

		user.get().setCurrentCasino(casino.get());
		return userRepository.save(user.get());
	}

	@PostMapping("/users/{uid}/games")
	public List<Game> bettableGames(@PathVariable long uid) {
		Optional<User> user = userRepository.findById(uid);
		if (!user.isPresent()) {
			throw new NotFoundException(String.valueOf(uid), "User");
		}

		if (user.get().getCurrentCasino() == null) {
			throw new RuntimeException("You did not enter in casino yet.");// need to handle api exception
		}
		return gameRepository.findByCasinoAndStatus(user.get().getCurrentCasino(), Status.open);
	}

	@PostMapping("/users/{uid}/bets")
	public BetOut bettableGames(@PathVariable long uid, @RequestBody @Valid BetIn betIn) {
		Optional<User> user = userRepository.findById(uid);
		if (!user.isPresent()) {
			throw new NotFoundException(String.valueOf(uid), "User");
		}

		if (user.get().getCurrentCasino() == null) {
			throw new RuntimeException("You did not enter in casino yet.");// need to handle api exception
		}
		Optional<Game> game = gameRepository.findById(betIn.getGameId());
		if (!game.isPresent()) {
			throw new NotFoundException(String.valueOf(betIn.getGameId()), "Game");
		}
		if (game.get().getStatus() != Status.open) {
			throw new RuntimeException("Game is already closed.");
		}

		if (betIn.getAmount() < 1) {
			throw new RuntimeException("Amount should be greter than 0.");
		}

		if (betIn.getAmount() > user.get().getBalance()) {
			throw new RuntimeException("You do have sufficient balance.");
		}
		BetNumber number = BetNumber.getFor(betIn.getNumber());

		if (number == null) {
			throw new RuntimeException("Invalid bet number.");
		}

		Bet bet = new Bet();
		bet.setGame(game.get());
		bet.setUser(user.get());
		bet.setCasino(user.get().getCurrentCasino());
		bet.setAmount(betIn.getAmount());
		bet.setBetNumber(number);
		betRepository.save(bet);
		return BetOut.getResp(bet);
	}

}
