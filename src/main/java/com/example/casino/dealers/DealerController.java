package com.example.casino.dealers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.casino.bets.Bet;
import com.example.casino.bets.BetRepository;
import com.example.casino.casinos.Casino;
import com.example.casino.casinos.CasinoRepository;
import com.example.casino.exception.NotFoundException;
import com.example.casino.games.Game;
import com.example.casino.games.GameRepository;
import com.example.casino.games.Status;
import com.example.casino.games.ThrowBallOut;
import com.example.casino.transactions.EntityType;
import com.example.casino.transactions.Transaction;
import com.example.casino.transactions.TransactionHelpter;
import com.example.casino.transactions.TransactionRepository;
import com.example.casino.transactions.Type;
import com.example.casino.users.User;
import com.example.casino.users.UserRepository;
import com.example.casino.util.DateUtil;
import com.example.casino.util.GameHelper;

@RestController
@RequestMapping("/api/v1")
public class DealerController {

	@Autowired
	private DealerRepository dealerRepository;

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private BetRepository betRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private TransactionRepository transactionRepository;

	@Autowired
	private CasinoRepository casinoRepository;

	@PostMapping("/dealers/{did}/start_game")
	public Game startGame(@PathVariable long did) {
		Optional<Dealer> dealer = dealerRepository.findById(did);
		if (!dealer.isPresent()) {
			throw new NotFoundException(String.valueOf(did), "Dealer");
		}
		Game currentGame = gameRepository.findByDealerAndStatus(dealer.get(), Status.open);
		if (currentGame != null) {
			throw new RuntimeException("already game running");
		}
		Game game = new Game();
		game.setDealer(dealer.get());
		game.setCasino(dealer.get().getCasino());
		game.setStatus(Status.open);
		game.setStartTime(DateUtil.sqlDateNow());
		return gameRepository.save(game);
	}

	@PostMapping("/dealers/{did}/throw_ball")
	public ThrowBallOut throwBall(@PathVariable long did) {
		Optional<Dealer> dealer = dealerRepository.findById(did);
		if (!dealer.isPresent()) {
			throw new NotFoundException(String.valueOf(did), "Dealer");
		}

		Game currentGame = gameRepository.findByDealerAndStatus(dealer.get(), Status.open);

		if (currentGame == null) {
			throw new RuntimeException("You did not start any game yet.");
		}
		if (currentGame.getDiceResult() != 0) {
			throw new RuntimeException("You have already thrown ball.");
		}
		currentGame.setDiceResult(GameHelper.getRandomNum());
		currentGame.setDiceRolledAt(DateUtil.sqlDateNow());
		gameRepository.save(currentGame);
		ThrowBallOut out = new ThrowBallOut();
		out.setDate(DateUtil.sqlDateNow());
		out.setNumber(10);
		out.setGame_id(currentGame.getId());
		return out;
	}

	@PostMapping("/dealers/{did}/finish_game")
	public Game finishGame(@PathVariable long did) {
		Optional<Dealer> dealer = dealerRepository.findById(did);
		if (!dealer.isPresent()) {
			throw new NotFoundException(String.valueOf(did), "Dealer");
		}
		Game currentGame = gameRepository.findByDealerAndStatus(dealer.get(), Status.open);
		if (currentGame == null) {
			throw new RuntimeException("There is no job to finish");
		}
		if (currentGame.getDiceResult() == 0) {
			throw new RuntimeException("You did not throw ball yet.");
		}
		List<Bet> bets = betRepository.findByGame(currentGame);
		int casinoAmt = 0;
		for (Bet bet : bets) {
			User user = bet.getUser();
			Transaction txn = TransactionHelpter.getTxn(EntityType.user, bet.getUser().getId(), Type.credit,
					bet.getAmount());
			if (GameHelper.isOwn(currentGame.getDiceResult(), bet.getBetNumber())) {
				user.setBalance(user.getBalance() + bet.getAmount());
				casinoAmt = casinoAmt - bet.getAmount();
			} else {
				user.setBalance(user.getBalance() - bet.getAmount());
				txn.setType(Type.debit);
				casinoAmt = casinoAmt + bet.getAmount();
			}
			txn.setGame(currentGame);
			txn.setCasino(currentGame.getCasino());
			transactionRepository.save(txn);
			userRepository.save(user);

		}
		Casino casino = currentGame.getCasino();
		casino.setBalance(casino.getBalance() + casinoAmt);
		casinoRepository.save(casino);
		currentGame.setEndTime(DateUtil.sqlDateNow());
		currentGame.setStatus(Status.finished);
		return gameRepository.save(currentGame);
	}

	@PostMapping("/dealers/{did}/cancle_game")
	public Game cancleGame(@PathVariable long did) {
		Optional<Dealer> dealer = dealerRepository.findById(did);
		if (!dealer.isPresent()) {
			throw new NotFoundException(String.valueOf(did), "Dealer");
		}
		Game currentGame = gameRepository.findByDealerAndStatus(dealer.get(), Status.open);
		if (currentGame == null) {
			throw new RuntimeException("You did not start any game yet.");
		}
		currentGame.setEndTime(DateUtil.sqlDateNow());
		currentGame.setStatus(Status.cancled);
		return gameRepository.save(currentGame);
	}

}
