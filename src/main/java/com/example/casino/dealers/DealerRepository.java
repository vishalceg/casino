package com.example.casino.dealers;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.casino.casinos.Casino;

@Repository("dealerRepository")
public interface DealerRepository extends JpaRepository<Dealer, Long> {

	List<Dealer> findByCasino(Casino casino);

}
